use std::env;
use std::io;
use std::path::Path;
use std::fs::{File, OpenOptions};
use serde::{Deserialize, Serialize};
use serde_json;
use argon2::{self, Config};


#[derive(Serialize,Deserialize)]
struct User {
    username: String,
    password: String,
    gemlog_root: String,
}

fn check_usernames(proposed: &String) -> Result<(), Box<dyn std::error::Error>> {
    let all_users: Vec<User> = {
        serde_json::from_str(include_str!("mindhoney/users.json")).unwrap()
    };
    let usernames: Vec<String> = {
        all_users.iter().filter(|x| x.username).collect();
    };
    if usernames.contains(*proposed) {
        Err("Username exists")?
    } else {
        let mut outfile = OpenOptions::new()
                          .append(true)
                          .create(true)
                          .open("mindhoney/users.json");
        let newuser: User = User {
            username: proposed,
            password: "unset",
            gemlog_root: "unset",
        };
        let jsonout = serde_json::to_string_pretty(newuser).unwrap();
        writeln!(outfile,"{}\n",jsonout)?;
        print("20 text/gemini\r\n");
        print("Username {} created.\n=> ./password Create password\n");
        Ok(())
    }
}

fn choose_username() {
    match env::var("QUERY_STRING") {
        Ok("") => print!("10 Choose your username:\r\n"),
        Ok(x) => if !check_usernames(&x).is_ok() {
            print!("10 That username is taken. Please try again:\r\n")
        },
        Err(_) => print!("51 Query String Invalid, Page Not Found\r\n"),
    };
}

fn add_password(proposed: &String) -> Result<(), Box<dyn std::error::Error>> {
    let all_users: Vec<User> = {
        serde_json::from_str(include_str!("mindhoney/users.json")).unwrap()
    };
    if all_users.len() == 0 {
        print!("51 No Users Found! Restart user creation.\r\n");
        return Err("No users")?
    }
    if all_users.last().password != "unset".to_owned() {
        print!("51 No Unset Password, Page Not Found\r\n");
        Err("No unset password")?
    } else {
        let config = Config::default();
        let natrium = include_bytes!("mindhoney/bin-cfg");
        let hash = argon2::hash_encoded(proposed,natrium,&config).unwrap();
        this_user = all_users.pop().unwrap();
        this_user.password = hash;
        let mut outfile = OpenOptions::new()
                          .append(true)
                          .open("mindhoney/users.json");
        let jsonout = serde_json::to_string_pretty(this_user).unwrap();
        writeln!(outfile,"{}\n",jsonout);
        print!("30 ../gemlogs\r\n");
    }
}

fn password() {
    match env::var("QUERY_STRING") {
        Ok("") => print!("11 Choose your password:\r\n"),
        Ok(x) => add_password(x),
        Err(_) => print!("51 Query String Invalid, Page Not Found\r\n"),
    };
}

fn display_path(currpath: &String) {
    if currpath.ends_with("1u1s1e1t1h1i1s1") {
        let all_users: Vec<User> = {
            serde_json::from_str(include_str!("mindhoney/users.json")).unwrap()
        };
        if all_users.len() == 0 {
            print!("51 No Users Found! Restart user creation.\r\n");
            return Err("No users")?
        } else {
            this_user = all_users.pop().unwrap();
            let correct_path = currpath.replace("1u1s1e1t1h1i1s1","");
            if this_user.gemlog_root == "unset" {
                this_user.gemlog_root = correct_path;
            } else {
                this_user.gemlog_root.push_str(":");
                this_user.gemlog_root.push_str(correct_path);
            }
            let mut outfile = OpenOptions::new()
                              .append(true)
                              .open("mindhoney/users.json");
            let jsonout = serde_json::to_string_pretty(this_user).unwrap();
            writeln!(outfile,"{}\n",jsonout);
            print!("20 text/gemini\r\n");
            println!("=> ../../mindhoney/create_admin/gemlogs Add another");
            println!("=> ../../mindhoney Done");
        }
    } else {
        print!("20 text/gemini\r\n");
        println!("=> ../../mindhoney?{}%2F1u1s1e1t1h1i1s1 Select",currpath);
        println!("=> ../../mindhoney?{}%2F.. ..",currpath);
        show_subdirs(&currpath);
    }
}

fn show_subdirs(currpath: &String) {
    path = Path::new(*currpath);
    if path.is_dir() {
        for entry in fs::read_dir(path)? {
            let subpath = entry.path();
            if subpath.is_dir() {
                println!("=> ../../mindhoney?{0}%2F{1} {1}",
                         *currpath,subpath.to_str().unrwap());
            }
        }
    }
}

fn choose_gemlogs() {
    if let Ok(x) = env::var("QUERY_STRING") {
        display_path(&x);
    } else {
        print!("51 Error inputting path\r\n");
    }
}

fn initialize() {
    let path_info = match env::var("PATH_INFO") {
        Ok(x) => x,
        Err(x) => print!("30 ..\r\n"); return x,
    };
    let comms: Vec<&str> = path_info.split("/").collect();
    match comms.len() {
        0 => print!("30 ..\r\n"),
        1..=2 => if comms[0] == "create_admin" {
            if comms.len() == 1 {
                choose_username();
            } else {
                match comms[1] {
                    "password" => create_password(),
                    "gemlogs" => choose_gemlogs(),
                    _ => print!("30 ..\r\n"),
                }
            }
        } else {
            print!("30 ..\r\n");
        },
    }
}
